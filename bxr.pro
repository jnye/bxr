TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    lmdb \
    msgpack \
    zlib \
    core \
    core-test \
    gui
