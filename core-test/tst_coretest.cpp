#include <QtTest>

#include "core.h"

class CoreTest : public QObject
{
    Q_OBJECT

public:
    CoreTest();
    ~CoreTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

CoreTest::CoreTest()
{

}

CoreTest::~CoreTest()
{

}

void CoreTest::initTestCase()
{

}

void CoreTest::cleanupTestCase()
{

}

void CoreTest::test_case1()
{
    doSomething();
}

QTEST_APPLESS_MAIN(CoreTest)

#include "tst_coretest.moc"
