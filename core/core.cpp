#include "core.h"

#include <string>
#include <iostream>
#include <sstream>

#include "lmdb.h"
#include "msgpack.hpp"
#include "zlib.h"

using namespace std;

void doSomething()
{
    z_stream strm;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    inflateInit(&strm);

    MDB_env *env;
    mdb_env_create(&env);
    mdb_env_close(env);

    msgpack::type::tuple<int, bool, std::string> src(1, true, "example");
    std::stringstream buffer;
    msgpack::pack(buffer, src);

    cout << "Did something!" << endl;
}
