#-------------------------------------------------
#
# Project created by QtCreator 2018-10-23T14:24:47
#
#-------------------------------------------------

QT       -= gui

TARGET = core
TEMPLATE = lib
CONFIG += warn_off

DEFINES += CORE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        core.cpp

HEADERS += \
        core.h \
        core_global.h 

unix {
    target.path = /usr/lib
    INSTALLS += target
}


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../lmdb/release/ -llmdb
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../lmdb/debug/ -llmdb
else:unix: LIBS += -L$$OUT_PWD/../lmdb/ -llmdb

INCLUDEPATH += $$PWD/../lmdb
DEPENDPATH += $$PWD/../lmdb

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lmdb/release/liblmdb.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lmdb/debug/liblmdb.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lmdb/release/lmdb.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../lmdb/debug/lmdb.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../lmdb/liblmdb.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../msgpack/release/ -lmsgpack
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../msgpack/debug/ -lmsgpack
else:unix: LIBS += -L$$OUT_PWD/../msgpack/ -lmsgpack

INCLUDEPATH += $$PWD/../msgpack/include
DEPENDPATH += $$PWD/../msgpack

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../msgpack/release/libmsgpack.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../msgpack/debug/libmsgpack.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../msgpack/release/msgpack.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../msgpack/debug/msgpack.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../msgpack/libmsgpack.a


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../zlib/release/ -lzlib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../zlib/debug/ -lzlib
else:unix: LIBS += -L$$OUT_PWD/../zlib/ -lzlib

INCLUDEPATH += $$PWD/../zlib
DEPENDPATH += $$PWD/../zlib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../zlib/release/libzlib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../zlib/debug/libzlib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../zlib/release/zlib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../zlib/debug/zlib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../zlib/libzlib.a
