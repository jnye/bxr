#include "mainwindow.h"
#include "settingsdialog.h"

#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QDir>

#include "core.h"

QString getSteamPath() {
        QSettings steamSettings("HKEY_CURRENT_USER\\Software\\Valve\\Steam", QSettings::NativeFormat);
        return steamSettings.value("SteamPath", "").toString();
}

int getSteamActiveProcessActiveUser() {
    QSettings steamActiveProcessSettings("HKEY_CURRENT_USER\\Software\\Valve\\Steam\\ActiveProcess", QSettings::NativeFormat);
    return steamActiveProcessSettings.value("ActiveUser", 0).toInt();
}

QString getSteamAppBoundlessInstallLocation() {
    QString installLocation;
#ifdef Q_OS_WIN
    QString appName = "Steam App 324510";
    // Search the 32-bit view of the registry
    QSettings reg32("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall", QSettings::Registry32Format);
    auto allGroups = reg32.childGroups();
    for (auto group : allGroups) {
        qInfo() << "Group:" << group;
        if (group != appName) continue;
        reg32.beginGroup(group);
        for (auto key : reg32.childKeys()) {
            if(key != "InstallLocation") continue;
            installLocation = reg32.value("InstallLocation").toString();
        }
        reg32.endGroup();
    }
    if (!installLocation.isEmpty()) {
        return installLocation;
    }
    // Search the 64-bit view of the registry
    QSettings reg64("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall", QSettings::Registry64Format);
    allGroups = reg64.childGroups();
    for (auto group : allGroups) {
        qInfo() << "Group:" << group;
        if (group != appName) continue;
        reg64.beginGroup(group);
        for (auto key : reg64.childKeys()) {
            if(key != "InstallLocation") continue;
            installLocation = reg64.value("InstallLocation").toString();
        }
        reg64.endGroup();
    }
#endif

#ifdef Q_OS_MACX
    QString path = QDir::homePath()
            .append("/Library/Application Support/Steam/steamapps/common/Boundless");
    QDir dir(path);
    if(dir.exists()) {
        installLocation = path;
    }
#endif

    return installLocation;
}

void autoDetectSettings()
{
    QSettings appSettings;
    QString gamePath = appSettings.value("GamePath", "").toString();
    if(gamePath.isEmpty()) {
        qInfo() << "No GamePath set, autodetecting";
        gamePath = getSteamAppBoundlessInstallLocation();
        if(!gamePath.isEmpty()) {
            appSettings.setValue("GamePath", gamePath);
            qInfo() << "Setting GamePath to:" << gamePath;
        }
    } else {
        qInfo() << "GamePath:" << gamePath;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Boundless Ninja");
    QCoreApplication::setOrganizationDomain("boundless.ninja");
    QCoreApplication::setApplicationName("bxr");

    QApplication application(argc, argv);

    MainWindow mainWindow;
    mainWindow.show();

    autoDetectSettings();

    return application.exec();
}
