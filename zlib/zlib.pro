#-------------------------------------------------
#
# Project created by QtCreator 2018-10-23T11:40:38
#
#-------------------------------------------------

QT       -= core gui

TARGET = zlib
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        adler32.c \
        compress.c \
        crc32.c \
        deflate.c \
        gzclose.c \
        gzlib.c \
        gzread.c\
        gzwrite.c \
        infback.c \
        inffast.c \
        inflate.c \
        inftrees.c \
        trees.c \
        uncompr.c \
        zutil.c

HEADERS += \
        zconf.h \
        zlib.h \
        crc32.h \
        deflate.h \
        zutil.h \
        gzguts.h \
        inftrees.h \
        inflate.h \
        inffast.h \
        trees.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
